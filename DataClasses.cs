﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Linq;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourseWork
{
    static class ListGovernment
    {
        public static List<Government> governments { get; set; }

        public static Government GetByName(string name)
        {
            return governments.Find(item => string.Equals(item.GName, name));
        }

        public static bool CheckUnique(string name)
        {
            foreach (Government item in governments)
                if (item.GName == name)
                    return false;

            return true;
        }

        public static bool DeleteGvn(Government government)
        {
            // удаление субъектов в государстве
            District district;
            for (int i = 0; i < ListDistrict.districts.Count;)
            {
                district = ListDistrict.districts[i];
                ++i;

                // удалить субъект, расположенный в государстве government
                if (government.GName == district.GName)
                    if (ListDistrict.DeleteDistrict(district))
                        --i;
            }

            if (governments.Remove(government))
                return true;
            return false;
        }

        public static void ResetPopulation()
        {
            foreach (Government government in governments)
                government.RecountPopulation();
        }
    }

    static class ListDistrict
    {
        public static List<District> districts { get; set; }

        public static District GetByName(string name)
        {
            return districts.Find(item => string.Equals(item.DistrictName, name));
        }

        public static bool CheckUnique(string nameGvn, string nameDistrict)
        {
            foreach (District item in districts)
                if (item.GName == nameGvn && item.DistrictName == nameDistrict)
                    return false;

            return true;
        }

        public static bool DeleteDistrict(District district)
        {
            // удаление районов в субъекте
            CityArea cityArea;
            for (int i = 0; i < ListCityArea.cityAreas.Count;)
            {
                cityArea = ListCityArea.cityAreas[i];
                ++i;

                // удалить район, который расплоложен в субъекте district
                if (cityArea.DistrictName == district.DistrictName &&
                    cityArea.GName == district.GName)
                    if (ListCityArea.DeleteCityArea(cityArea))
                        --i;
            }

            if (districts.Remove(district))
                return true;

            return false;
        }

        /// <summary>
        /// Возвращате площадь всех субъектов расположенных в государстве government.
        /// </summary>
        public static int GetTotalSquare(Government government)
        {
            int sum = 0;
            foreach (District subject in districts)
                if (subject.GName == government.GName)
                    sum += subject.DistrictSquare;
            return sum;
        }

        public static void ResetDistrictData(Government government)
        {
            foreach (District district in districts)
                if (district.GName == government.GName)
                {
                    district.GSquare = government.GSquare;
                    ListCityArea.ResetCityData(district);
                }
        }

        public static void ResetNameGovernment(Government oldGvn, Government newGvn)
        {
            foreach (District district in districts)
                if (district.GName == oldGvn.GName)
                    district.GName = newGvn.GName;
        }

        public static void ResetPopulation()
        {
            foreach (District district in districts)
                district.RecountPopulation();
        }

        /// <summary>
        /// Проверяет целостность ссылок на государство у всех субъектов.
        /// </summary>
        /// <returns></returns>
        public static bool CheckIntegrityLink()
        {
            foreach (District district in districts)
                if (ListGovernment.GetByName(district.GName) == null)
                    return false;
            return true;
        }
    }

    static class ListCityArea
    {
        public static List<CityArea> cityAreas { get; set; }

        public static CityArea GetByName(string name)
        {
            return cityAreas.Find(item => string.Equals(item.CityName, name));
        }

        public static bool CheckUnique(string nameGvn, string nameDistrict, string nameCity)
        {
            foreach (CityArea item in cityAreas)
                if (item.GName == nameGvn && item.DistrictName == nameDistrict && item.CityName == nameCity)
                    return false;

            return true;
        }

        public static bool DeleteCityArea(CityArea cityArea)
        {
            // удаляет район вместе с населенными пунктами
            Government government = ListGovernment.GetByName(cityArea.GName);
            District district = ListDistrict.GetByName(cityArea.DistrictName);

            // удаление населенных пунктов
            Locality locality;
            for (int i = 0; i < ListLocality.localities.Count;)
            {
                locality = ListLocality.localities[i];
                ++i;

                // удалить населенный пункт, расположенный в районе
                if (locality.CityName == cityArea.CityName &&
                    locality.DistrictName == cityArea.DistrictName &&
                    locality.GName == cityArea.GName)
                {
                    if (ListLocality.localities.Remove(locality))
                    {
                        // уменьшить население
                        --i;
                        government.AddThisPopulation(-locality.LocPopulation);
                        district.AddThisPopulation(-locality.LocPopulation);
                        cityArea.AddThisPopulation(-locality.LocPopulation);
                    }
                }
            }

            if (cityAreas.Remove(cityArea))
                return true;

            return false;
        }

        /// <summary>
        /// Возвращате площадь всех районов расположенных в субъекте district.
        /// </summary>
        public static int GetTotalSquare(District district)
        {
            int sum = 0;
            foreach (CityArea cityArea in cityAreas)
                if (cityArea.DistrictName == district.DistrictName &&
                    cityArea.GName == district.GName)
                    sum += cityArea.CitySquare;
            return sum;
        }

        public static void ResetCityData(District district)
        {
            foreach (CityArea cityArea in cityAreas)
                if (cityArea.DistrictName == district.DistrictName &&
                    cityArea.GName == district.GName)
                {
                    cityArea.DistrictSquare = district.DistrictSquare;
                    ListLocality.ResetLocData(cityArea);
                }
        }

        public static void ResetNameGovernment(Government oldGvn, Government newGvn)
        {
            foreach (CityArea city in cityAreas)
                if (city.GName == oldGvn.GName)
                    city.GName = newGvn.GName;
        }

        public static void ResetNameDistrict(District oldD, District newD)
        {
            foreach (CityArea city in cityAreas)
                if (city.DistrictName == oldD.DistrictName)
                    city.DistrictName = newD.DistrictName;
        }

        public static void ResetPopulation()
        {
            foreach (CityArea city in cityAreas)
                city.RecountPopulation();
        }

        public static bool CheckIntegrityLink()
        {
            foreach (CityArea cityArea in cityAreas)
            {
                if (ListGovernment.GetByName(cityArea.GName) == null)
                    return false;
                if (ListDistrict.GetByName(cityArea.DistrictName) == null)
                    return false;
            }
            return true;
        }
    }

    static class ListLocality
    {
        public static List<Locality> localities { get; set; }

        public static Locality GetByName(string name)
        {
            return localities.Find(item => string.Equals(item.LocName, name));
        }

        public static bool DeleteLocality(Locality locality)
        {
            if (localities.Remove(locality))
            {
                // уменьшить население
                Government government = ListGovernment.GetByName(locality.GName);
                District district = ListDistrict.GetByName(locality.DistrictName);
                CityArea city = ListCityArea.GetByName(locality.CityName);
                government.AddThisPopulation(-locality.LocPopulation);
                district.AddThisPopulation(-locality.LocPopulation);
                city.AddThisPopulation(-locality.LocPopulation);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Возвращате площадь всех населенных пунктов расположенных в районе cityArea.
        /// </summary>
        public static int GetTotalSquare(CityArea cityArea)
        {
            int sum = 0;
            foreach (Locality locality in localities)
                if (locality.CityName == cityArea.CityName &&
                    locality.DistrictName == cityArea.DistrictName &&
                    locality.GName == cityArea.GName)
                    sum += locality.LocSquare;
            return sum;
        }

        /// <summary>
        /// Перезаписывает данные полей CitySquare во всех Locality,
        /// расположенных в cityArea.
        /// </summary>
        public static void ResetLocData(CityArea cityArea)
        {
            foreach (Locality locality in localities)
                if (locality.CityName == cityArea.CityName &&
                    locality.DistrictName == cityArea.DistrictName &&
                    locality.GName == cityArea.GName)
                    locality.CitySquare = cityArea.CitySquare;
        }

        public static void ResetNameGovernment(Government oldGvn, Government newGvn)
        {
            foreach (Locality loc in localities)
                if (loc.GName == oldGvn.GName)
                    loc.GName = newGvn.GName;
        }

        public static void ResetNameDistrict(District oldD, District newD)
        {
            foreach (Locality loc in localities)
                if (loc.DistrictName == oldD.DistrictName)
                    loc.DistrictName = newD.DistrictName;
        }

        public static void ResetNameCityArea(CityArea oldCity, CityArea newCity)
        {
            foreach (Locality loc in localities)
                if (loc.CityName == oldCity.CityName)
                    loc.CityName = newCity.CityName;
        }

        public static bool CheckIntegrityLink()
        {
            foreach (Locality locality in localities)
            {
                if (ListGovernment.GetByName(locality.GName) == null)
                    return false;
                if (ListDistrict.GetByName(locality.DistrictName) == null)
                    return false;
                if (ListCityArea.GetByName(locality.CityName) == null)
                    return false;
            }
            return true;
        }
    }

    public static class DataSerializer
    {
        // Методы сериализации данных в xml
        #region
        private static XElement SerializeGovernment(in Government obj)
        {
            XElement xElement = new XElement("Government");
            xElement.Add(new XElement("GovernmentName", obj.GName));
            xElement.Add(new XElement("GovernmentSquare", obj.GSquare));
            return xElement;
        }

        private static XElement SerializeDistrict(in District obj)
        {
            XElement xElement = SerializeGovernment((Government)obj);
            xElement.Name = "District";
            xElement.Add(new XElement("DistrictName", obj.DistrictName));
            xElement.Add(new XElement("DistrictSquare", obj.DistrictSquare));
            return xElement;
        }

        private static XElement SerializeCityArea(in CityArea obj)
        {
            XElement xElement = SerializeDistrict((District)obj);
            xElement.Name = "CityArea";
            xElement.Add(new XElement("CityAreaName", obj.CityName));
            xElement.Add(new XElement("CityAreaSquare", obj.CitySquare));
            return xElement;
        }

        private static XElement SerializeLocality(in Locality obj)
        {
            XElement xElement = SerializeCityArea((CityArea)obj);
            xElement.Name = "Locality";
            xElement.Add(new XElement("LocalityName", obj.LocName));
            xElement.Add(new XElement("LocalitySquare", obj.LocSquare));
            xElement.Add(new XElement("LocalityPopulation", obj.LocPopulation));
            xElement.Add(new XElement("LocalityType", obj.LocType));
            return xElement;
        }

        /// <summary>
        /// Сохраняет все данные из списков в файл path. Перед вызовом необходимо убедиться,
        /// что path является действительным путем.
        /// </summary>
        public static void SaveXML(string path)
        {
            XDocument xDocument = new XDocument();
            XElement xDATA = new XElement("DATA");

            XElement xGvns = new XElement("Governments");
            foreach (Government gvn in ListGovernment.governments)
            {
                XElement xGvn = SerializeGovernment(in gvn);
                xGvns.Add(xGvn);
            }

            XElement xDistricts = new XElement("Districts");
            foreach (District district in ListDistrict.districts)
            {
                XElement xDistrict = SerializeDistrict(district);
                xDistricts.Add(xDistrict);
            }

            XElement xCityAreas = new XElement("CityAreas");
            foreach (CityArea cityArea in ListCityArea.cityAreas)
            {
                XElement xCityArea = SerializeCityArea(cityArea);
                xCityAreas.Add(xCityArea);
            }

            XElement xLocalities = new XElement("Localities");
            foreach (Locality locality in ListLocality.localities)
            {
                XElement xLocality = SerializeLocality(locality);
                xLocalities.Add(xLocality);
            }

            xDATA.Add(xGvns);
            xDATA.Add(xDistricts);
            xDATA.Add(xCityAreas);
            xDATA.Add(xLocalities);
            xDocument.Add(xDATA);
            xDocument.Save(path);
        }
        #endregion

        // Методы десериализации данных из xml
        #region
        private static void DeserializeGovernments(XmlNodeList nodeList)
        {
            try
            {
                foreach (XmlNode node in nodeList)
                {
                    string GvnName = null;
                    int square = 0;

                    foreach (XmlNode childNode in node.ChildNodes)
                    {
                        if (childNode.Name == "GovernmentName")
                            GvnName = childNode.InnerText;
                        else if (childNode.Name == "GovernmentSquare")
                            int.TryParse(childNode.InnerText, out square);
                    }

                    Government government = new Government(GvnName, square);
                    ListGovernment.governments.Add(government);
                }
            }
            catch
            {
                throw new Exception("Не удалось преобразовать данные о государтсве," +
                    "возможно файл был повреждён.");
            }
        }

        private static void DeserializeDistricts(XmlNodeList nodeList)
        {
            try
            {
                foreach (XmlNode node in nodeList)
                {
                    string GvnName, dName;
                    int gSquare, dSquare;
                    GvnName = dName = null;
                    gSquare = dSquare = 0;

                    foreach (XmlNode childNode in node.ChildNodes)
                    {
                        if (childNode.Name == "GovernmentName")
                            GvnName = childNode.InnerText;
                        else if (childNode.Name == "GovernmentSquare")
                            int.TryParse(childNode.InnerText, out gSquare);
                        else if (childNode.Name == "DistrictName")
                            dName = childNode.InnerText;
                        else if (childNode.Name == "DistrictSquare")
                            int.TryParse(childNode.InnerText, out dSquare);
                    }

                    if (gSquare <= 0 || dSquare <= 0)
                        throw new Exception();

                    Government government = new Government(GvnName, gSquare);
                    District district = new District(government, dName, dSquare);
                    ListDistrict.districts.Add(district);
                }
            }
            catch
            {
                throw new Exception("Не удалось преобразовать данные о субъекте, возможно файл был повреждён.");
            }
        }

        private static void DeserializeCityAreas(XmlNodeList nodeList)
        {
            try
            {
                foreach (XmlNode node in nodeList)
                {
                    string GvnName, dName, cName;
                    int gSquare, dSquare, cSquare;
                    GvnName = dName = cName = null;
                    gSquare = dSquare = cSquare = 0;

                    foreach (XmlNode childNode in node.ChildNodes)
                    {
                        if (childNode.Name == "GovernmentName")
                            GvnName = childNode.InnerText;
                        else if (childNode.Name == "GovernmentSquare")
                            int.TryParse(childNode.InnerText, out gSquare);
                        else if (childNode.Name == "DistrictName")
                            dName = childNode.InnerText;
                        else if (childNode.Name == "DistrictSquare")
                            int.TryParse(childNode.InnerText, out dSquare);
                        else if (childNode.Name == "CityAreaName")
                            cName = childNode.InnerText;
                        else if (childNode.Name == "CityAreaSquare")
                            int.TryParse(childNode.InnerText, out cSquare);
                    }

                    if (gSquare <= 0 || dSquare <= 0 || cSquare <= 0)
                        throw new Exception();

                    Government government = new Government(GvnName, gSquare);
                    District district = new District(government, dName, dSquare);
                    CityArea cityArea = new CityArea(government, district, cName, cSquare);
                    ListCityArea.cityAreas.Add(cityArea);
                }
            }
            catch
            {
                throw new Exception("Не удалось преобразовать данные о районе," +
                    "возможно файл был повреждён.");
            }
        }

        private static void DeserializeLocalities(XmlNodeList nodeList)
        {
            try
            {
                foreach (XmlNode node in nodeList)
                {
                    string GvnName, dName, cName, lName, lType;
                    int gSquare, dSquare, cSquare, lSquare, lPopulation;
                    GvnName = dName = cName = lName = lType = null;
                    gSquare = dSquare = cSquare = lSquare = lPopulation = 0;

                    foreach (XmlNode childNode in node.ChildNodes)
                    {
                        if (childNode.Name == "GovernmentName")
                            GvnName = childNode.InnerText;
                        else if (childNode.Name == "GovernmentSquare")
                            int.TryParse(childNode.InnerText, out gSquare);
                        else if (childNode.Name == "DistrictName")
                            dName = childNode.InnerText;
                        else if (childNode.Name == "DistrictSquare")
                            int.TryParse(childNode.InnerText, out dSquare);
                        else if (childNode.Name == "CityAreaName")
                            cName = childNode.InnerText;
                        else if (childNode.Name == "CityAreaSquare")
                            int.TryParse(childNode.InnerText, out cSquare);
                        else if (childNode.Name == "LocalityName")
                            lName = childNode.InnerText;
                        else if (childNode.Name == "LocalitySquare")
                            int.TryParse(childNode.InnerText, out lSquare);
                        else if (childNode.Name == "LocalityPopulation")
                            int.TryParse(childNode.InnerText, out lPopulation);
                        else if (childNode.Name == "LocalityType")
                            lType = childNode.InnerText;
                    }

                    if (gSquare <= 0 || dSquare <= 0 || cSquare <= 0 || lSquare <= 0)
                        throw new Exception();

                    Government government = new Government(GvnName, gSquare);
                    District district = new District(government, dName, dSquare);
                    CityArea cityArea = new CityArea(government, district, cName, cSquare);
                    Locality locality = new Locality(government, district, cityArea, lName,
                        lType, lSquare, lPopulation);
                    ListLocality.localities.Add(locality);
                }
            }
            catch
            {
                throw new Exception("Не удалось преобразовать данные о населенном пункте," +
                    "возможно файл был повреждён.");
            }
        }

        /// <summary>
        /// Загружает данные из файла path и записывает в списки. Путь path должен
        /// быть действительным путем. Функция возбуждает исключение, если ей не удалось
        /// десереализовать данные из файла, необходимо обрабатывать при вызове.
        /// </summary>
        public static void LoadXML(string path)
        {
            XmlDocument document = new XmlDocument();
            document.Load(path);

            XmlElement xDATA = document.DocumentElement;

            if (xDATA.HasChildNodes)
            {
                foreach (XmlElement xNode in xDATA)
                {
                    if (xNode.Name == "Governments")
                        if (xNode.HasChildNodes)
                            DeserializeGovernments(xNode.ChildNodes);

                    if (xNode.Name == "Districts")
                        if (xNode.HasChildNodes)
                            DeserializeDistricts(xNode.ChildNodes);

                    if (xNode.Name == "CityAreas")
                        if (xNode.HasChildNodes)
                            DeserializeCityAreas(xNode.ChildNodes);

                    if (xNode.Name == "Localities")
                        if (xNode.HasChildNodes)
                            DeserializeLocalities(xNode.ChildNodes);
                }

                ListGovernment.ResetPopulation();
                ListDistrict.ResetPopulation();
                ListCityArea.ResetPopulation();
            }
        }
        #endregion
    }
}
