﻿using System;
using System.Windows.Forms;

namespace CourseWork
{
    public partial class FormGovernment : Form
    {
        public Government government { get; set; }
        private Government OldGvn = null;
        public FormGovernment()
        {
            InitializeComponent();
        }

        public FormGovernment(Government government)
        {
            InitializeComponent();
            textBoxGName.Text = government.GName;
            numericUpDown1.Value = government.GSquare;

            OldGvn = new Government(government);
            ListGovernment.governments.Remove(government);
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            string output;
            if (textBoxGName.Text == "")
            {
                output = String.Format("Поле \"{0}\" не должно быть пустым!", label1.Text);
                MessageBox.Show(output);
                return;
            }
            if (numericUpDown1.Value == 0)
            {
                output = String.Format("Поле \"{0}\" не должно быть пустым!", label2.Text);
                MessageBox.Show(output);
                return;
            }
            if (OldGvn == null && !ListGovernment.CheckUnique(textBoxGName.Text))
            {
                MessageBox.Show("Государство с таким наименованием уже существует!");
                return;
            }
            if (OldGvn != null)
            {
                int square = ListDistrict.GetTotalSquare(OldGvn);
                if (numericUpDown1.Value < square)
                {
                    output = String.Format("Указанная площадь государства меньше, чем сумма площадей " +
                        "субъектов, расположенных в этом государстве! Сумма площадей " +
                        "субъектов: {0}.", square);
                    MessageBox.Show(output, "Неверное значение поля");
                    return;
                }
            }


            government = new Government(textBoxGName.Text, (int)numericUpDown1.Value);
            if (OldGvn != null)
            {
                government.AddThisPopulation(OldGvn.GPopulation);
                ListDistrict.ResetDistrictData(government);
                if (OldGvn.GName != government.GName)
                {
                    ListDistrict.ResetNameGovernment(OldGvn, government);
                    ListCityArea.ResetNameGovernment(OldGvn, government);
                    ListLocality.ResetNameGovernment(OldGvn, government);
                }
            }

            DialogResult = DialogResult.OK;
        }
    }
}
