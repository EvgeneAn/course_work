﻿using System;
using System.Windows.Forms;

namespace CourseWork
{
    public partial class FormArea : Form
    {
        public CityArea cityArea { get; set; }
        private CityArea oldCityArea = null;
        public FormArea()
        {
            InitializeComponent();

            foreach (Government item in ListGovernment.governments)
                comboBoxG.Items.Add(item.GName);
            if (comboBoxG.Items.Count > 0)
                comboBoxG.SelectedIndex = 0;

            SetComboBoxSubject();
        }

        public FormArea(CityArea cityA)
        {
            InitializeComponent();

            foreach (Government item in ListGovernment.governments)
                comboBoxG.Items.Add(item.GName);

            SetComboBoxSubject();

            // заполнение полей первоночальными данными
            for (int i = 0; i < comboBoxG.Items.Count; ++i)
                if (comboBoxG.Items[i].ToString() == cityA.GName)
                {
                    comboBoxG.SelectedIndex = i;
                    break;
                }
            for (int i = 0; i < comboBoxS.Items.Count; ++i)
                if (comboBoxS.Items[i].ToString() == cityA.DistrictName)
                {
                    comboBoxS.SelectedIndex = i;
                    break;
                }

            textBox1.Text = cityA.CityName;
            numericUpDown1.Value = cityA.CitySquare;

            // сделать некоторые поля неизменяемыми
            comboBoxG.Enabled = false;
            comboBoxS.Enabled = false;

            oldCityArea = new CityArea(cityA);
            ListCityArea.cityAreas.Remove(cityA);
        }

        private void SetComboBoxSubject()
        {
            comboBoxS.Items.Clear();
            comboBoxS.Text = "";
            object obj = comboBoxG.SelectedItem;

            if (obj != null)
            {
                Government government = ListGovernment.GetByName(obj.ToString());
                foreach (District item in ListDistrict.districts)
                    if (item.GName == government.GName)
                        comboBoxS.Items.Add(item.DistrictName);
                if (comboBoxS.Items.Count > 0)
                    comboBoxS.SelectedIndex = 0;
            }
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            string output;
            if (textBox1.Text == "")
            {
                output = String.Format("Поле \"{0}\" не должно быть пустым!", label3.Text);
                MessageBox.Show(output, "Неверное значение поля");
                return;
            }
            if (numericUpDown1.Value == 0)
            {
                output = String.Format("Поле \"{0}\" не должно быть пустым!", label4.Text);
                MessageBox.Show(output, "Неверное значение поля");
                return;
            }
            if (oldCityArea != null)
            {
                int square = ListLocality.GetTotalSquare(oldCityArea);
                if (numericUpDown1.Value < square)
                {
                    output = String.Format("Указанная площадь района меньше, чем сумма площадей " +
                        "населенных пунктов, расположенных в этом районе! Сумма площадей " +
                        "населенных пунктов: {0}.", square);
                    MessageBox.Show(output, "Неверное значение поля");
                    return;
                }
            }


            object obj = comboBoxG.SelectedItem;
            if (obj != null)
            {
                Government government = ListGovernment.GetByName(obj.ToString());

                obj = comboBoxS.SelectedItem;
                if (obj != null)
                {
                    District district = ListDistrict.GetByName(obj.ToString());

                    if (ListCityArea.CheckUnique(government.GName, district.DistrictName, textBox1.Text))
                    {
                        cityArea = new CityArea(government, district, textBox1.Text, (int)numericUpDown1.Value);

                        if (cityArea.CitySquare == 0)
                        {
                            output = "Площадь района превышает площадь субъекта!" +
                                String.Format(" Осталось нераспределенной площади {0}.",
                                district.GetDistrictRemains());
                            MessageBox.Show(output, "Неверное значение поля");
                            return;
                        }

                        // при редактировании
                        if (oldCityArea != null)
                        {
                            cityArea.AddThisPopulation(oldCityArea.CityPopulation);
                            ListLocality.ResetLocData(cityArea);
                            if (oldCityArea.CityName != cityArea.CityName)
                                ListLocality.ResetNameCityArea(oldCityArea, cityArea);
                        }

                        DialogResult = DialogResult.OK;
                    }
                    else
                    {
                        output = "Район с таким наименованием уже существует в субъекте" +
                            String.Format(" {0}!", district.DistrictName);
                        MessageBox.Show(output, "Неверное значение поля");
                    }
                }
                else
                {
                    output = String.Format("Убедитесь, что поле {0} заполнено!", label2.Text);
                    MessageBox.Show(output, "Неверное значение поля");
                }

            }
            else
            {
                output = String.Format("Убедитесь, что поле {0} заполнено!", label1.Text);
                MessageBox.Show(output, "Неверное значение поля");
            }
        }

        private void comboBoxG_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetComboBoxSubject();
        }

        private void buttonCancle_Click(object sender, EventArgs e)
        {
            // если производилось редактирование и нажали "Отмена"
            // вернуть площадь и население из старых данных
            if (oldCityArea != null)
            {
                cityArea = oldCityArea;
                ListCityArea.cityAreas.Add(cityArea);
            }
        }
    }
}
