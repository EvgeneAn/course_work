﻿using System;
using System.Windows.Forms;

namespace CourseWork
{
    public partial class Calculator : UserControl
    {
        public Calculator()
        {
            InitializeComponent();
            textBoxN1.Text = "1";
            textBoxN2.Text = "1";
            comboBox1.SelectedIndex = 0;
            Calculate();
            // код из Calculator.Designer.cs
            // для того, чтобы не вызывались события при инициализации
            this.textBoxN1.TextChanged += new System.EventHandler(this.textBoxN1_TextChanged);
            this.textBoxN2.TextChanged += new System.EventHandler(this.textBoxN2_TextChanged);
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
        }

        private void Calculate()
        {
            bool parse;
            int n1, n2;
            parse = int.TryParse(textBoxN1.Text, out n1);
            parse = (int.TryParse(textBoxN2.Text, out n2) & parse) ? true : false;

            if (parse)
            {
                switch (comboBox1.SelectedIndex)
                {
                    case 0: // +
                        labelResponse.Text = (n1 + n2).ToString();
                        break;
                    case 1: // -
                        labelResponse.Text = (n1 - n2).ToString();
                        break;
                    case 2: // *
                        labelResponse.Text = (n1 * n2).ToString();
                        break;
                    case 3: // /
                        if (n2 == 0)
                        {
                            MessageBox.Show("Нельзя делить на ноль!",
                                "Неккоректные данные", MessageBoxButtons.OK,
                                MessageBoxIcon.Warning);
                            break;
                        }
                        labelResponse.Text = (n1 / n2).ToString();
                        break;
                }
            }
            else
                MessageBox.Show("Введены неккоректные данные!", "Ошибка поля",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void textBoxN1_TextChanged(object sender, EventArgs e)
        {
            Calculate();
        }

        private void textBoxN2_TextChanged(object sender, EventArgs e)
        {
            Calculate();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Calculate();
        }
    }
}
