﻿
namespace CourseWork
{
    partial class FormLocality
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelGovernment = new System.Windows.Forms.Label();
            this.labelSubject = new System.Windows.Forms.Label();
            this.labelCityArea = new System.Windows.Forms.Label();
            this.comboBoxCityArea = new System.Windows.Forms.ComboBox();
            this.labelName = new System.Windows.Forms.Label();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.labelSquare = new System.Windows.Forms.Label();
            this.numericUpDownSquare = new System.Windows.Forms.NumericUpDown();
            this.labelPopulation = new System.Windows.Forms.Label();
            this.numericUpDownPopulation = new System.Windows.Forms.NumericUpDown();
            this.buttonCancle = new System.Windows.Forms.Button();
            this.buttonOk = new System.Windows.Forms.Button();
            this.comboBoxG = new System.Windows.Forms.ComboBox();
            this.comboBoxSubject = new System.Windows.Forms.ComboBox();
            this.labelType = new System.Windows.Forms.Label();
            this.comboBoxType = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSquare)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPopulation)).BeginInit();
            this.SuspendLayout();
            // 
            // labelGovernment
            // 
            this.labelGovernment.AutoSize = true;
            this.labelGovernment.Location = new System.Drawing.Point(13, 13);
            this.labelGovernment.Name = "labelGovernment";
            this.labelGovernment.Size = new System.Drawing.Size(74, 13);
            this.labelGovernment.TabIndex = 0;
            this.labelGovernment.Text = "Государство:";
            // 
            // labelSubject
            // 
            this.labelSubject.AutoSize = true;
            this.labelSubject.Location = new System.Drawing.Point(35, 47);
            this.labelSubject.Name = "labelSubject";
            this.labelSubject.Size = new System.Drawing.Size(52, 13);
            this.labelSubject.TabIndex = 2;
            this.labelSubject.Text = "Субъект:";
            // 
            // labelCityArea
            // 
            this.labelCityArea.AutoSize = true;
            this.labelCityArea.Location = new System.Drawing.Point(46, 82);
            this.labelCityArea.Name = "labelCityArea";
            this.labelCityArea.Size = new System.Drawing.Size(41, 13);
            this.labelCityArea.TabIndex = 4;
            this.labelCityArea.Text = "Район:";
            // 
            // comboBoxCityArea
            // 
            this.comboBoxCityArea.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxCityArea.FormattingEnabled = true;
            this.comboBoxCityArea.Location = new System.Drawing.Point(93, 79);
            this.comboBoxCityArea.Name = "comboBoxCityArea";
            this.comboBoxCityArea.Size = new System.Drawing.Size(309, 21);
            this.comboBoxCityArea.TabIndex = 3;
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Location = new System.Drawing.Point(1, 117);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(86, 13);
            this.labelName.TabIndex = 6;
            this.labelName.Text = "Наименование:";
            // 
            // textBoxName
            // 
            this.textBoxName.Location = new System.Drawing.Point(93, 114);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(309, 20);
            this.textBoxName.TabIndex = 4;
            // 
            // labelSquare
            // 
            this.labelSquare.AutoSize = true;
            this.labelSquare.Location = new System.Drawing.Point(1, 182);
            this.labelSquare.Name = "labelSquare";
            this.labelSquare.Size = new System.Drawing.Size(86, 13);
            this.labelSquare.TabIndex = 8;
            this.labelSquare.Text = "Площадь (км2):";
            // 
            // numericUpDownSquare
            // 
            this.numericUpDownSquare.Location = new System.Drawing.Point(93, 180);
            this.numericUpDownSquare.Maximum = new decimal(new int[] {
            1000000000,
            0,
            0,
            0});
            this.numericUpDownSquare.Name = "numericUpDownSquare";
            this.numericUpDownSquare.Size = new System.Drawing.Size(309, 20);
            this.numericUpDownSquare.TabIndex = 6;
            // 
            // labelPopulation
            // 
            this.labelPopulation.AutoSize = true;
            this.labelPopulation.Location = new System.Drawing.Point(21, 217);
            this.labelPopulation.Name = "labelPopulation";
            this.labelPopulation.Size = new System.Drawing.Size(66, 13);
            this.labelPopulation.TabIndex = 10;
            this.labelPopulation.Text = "Население:";
            // 
            // numericUpDownPopulation
            // 
            this.numericUpDownPopulation.Location = new System.Drawing.Point(93, 215);
            this.numericUpDownPopulation.Maximum = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            this.numericUpDownPopulation.Name = "numericUpDownPopulation";
            this.numericUpDownPopulation.Size = new System.Drawing.Size(309, 20);
            this.numericUpDownPopulation.TabIndex = 7;
            // 
            // buttonCancle
            // 
            this.buttonCancle.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancle.Location = new System.Drawing.Point(12, 250);
            this.buttonCancle.Name = "buttonCancle";
            this.buttonCancle.Size = new System.Drawing.Size(75, 23);
            this.buttonCancle.TabIndex = 8;
            this.buttonCancle.Text = "Отмена";
            this.buttonCancle.UseVisualStyleBackColor = true;
            this.buttonCancle.Click += new System.EventHandler(this.buttonCancle_Click);
            // 
            // buttonOk
            // 
            this.buttonOk.Location = new System.Drawing.Point(93, 250);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(75, 23);
            this.buttonOk.TabIndex = 9;
            this.buttonOk.Text = "Применить";
            this.buttonOk.UseVisualStyleBackColor = true;
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // comboBoxG
            // 
            this.comboBoxG.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxG.FormattingEnabled = true;
            this.comboBoxG.Location = new System.Drawing.Point(93, 10);
            this.comboBoxG.Name = "comboBoxG";
            this.comboBoxG.Size = new System.Drawing.Size(309, 21);
            this.comboBoxG.TabIndex = 1;
            this.comboBoxG.SelectedIndexChanged += new System.EventHandler(this.comboBoxG_SelectedIndexChanged);
            // 
            // comboBoxSubject
            // 
            this.comboBoxSubject.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxSubject.FormattingEnabled = true;
            this.comboBoxSubject.Location = new System.Drawing.Point(93, 44);
            this.comboBoxSubject.Name = "comboBoxSubject";
            this.comboBoxSubject.Size = new System.Drawing.Size(309, 21);
            this.comboBoxSubject.TabIndex = 2;
            this.comboBoxSubject.SelectedIndexChanged += new System.EventHandler(this.comboBoxSubject_SelectedIndexChanged);
            // 
            // labelType
            // 
            this.labelType.AutoSize = true;
            this.labelType.Location = new System.Drawing.Point(58, 148);
            this.labelType.Name = "labelType";
            this.labelType.Size = new System.Drawing.Size(29, 13);
            this.labelType.TabIndex = 16;
            this.labelType.Text = "Тип:";
            // 
            // comboBoxType
            // 
            this.comboBoxType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxType.FormattingEnabled = true;
            this.comboBoxType.Items.AddRange(new object[] {
            "Город",
            "Село",
            "Поселок городского типа"});
            this.comboBoxType.Location = new System.Drawing.Point(93, 145);
            this.comboBoxType.Name = "comboBoxType";
            this.comboBoxType.Size = new System.Drawing.Size(309, 21);
            this.comboBoxType.TabIndex = 5;
            // 
            // FormLocality
            // 
            this.AcceptButton = this.buttonOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.buttonCancle;
            this.ClientSize = new System.Drawing.Size(414, 287);
            this.Controls.Add(this.comboBoxType);
            this.Controls.Add(this.labelType);
            this.Controls.Add(this.comboBoxSubject);
            this.Controls.Add(this.comboBoxG);
            this.Controls.Add(this.buttonOk);
            this.Controls.Add(this.buttonCancle);
            this.Controls.Add(this.numericUpDownPopulation);
            this.Controls.Add(this.labelPopulation);
            this.Controls.Add(this.numericUpDownSquare);
            this.Controls.Add(this.labelSquare);
            this.Controls.Add(this.textBoxName);
            this.Controls.Add(this.labelName);
            this.Controls.Add(this.comboBoxCityArea);
            this.Controls.Add(this.labelCityArea);
            this.Controls.Add(this.labelSubject);
            this.Controls.Add(this.labelGovernment);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormLocality";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Добавление/редактирование населенного пункта";
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSquare)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPopulation)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelGovernment;
        private System.Windows.Forms.Label labelSubject;
        private System.Windows.Forms.Label labelCityArea;
        private System.Windows.Forms.ComboBox comboBoxCityArea;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.Label labelSquare;
        private System.Windows.Forms.NumericUpDown numericUpDownSquare;
        private System.Windows.Forms.Label labelPopulation;
        private System.Windows.Forms.NumericUpDown numericUpDownPopulation;
        private System.Windows.Forms.Button buttonCancle;
        private System.Windows.Forms.Button buttonOk;
        private System.Windows.Forms.ComboBox comboBoxG;
        private System.Windows.Forms.ComboBox comboBoxSubject;
        private System.Windows.Forms.Label labelType;
        private System.Windows.Forms.ComboBox comboBoxType;
    }
}