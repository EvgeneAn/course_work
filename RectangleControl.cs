﻿using System;
using System.Windows.Forms;

namespace CourseWork
{
    public partial class RectangleControl : UserControl
    {
        public RectangleControl()
        {
            InitializeComponent();
            Calculate();
        }

        private decimal Square()
        {
            return (decimal)(numericUpDown1.Value * numericUpDown2.Value);
        }

        private decimal Perimetr()
        {
            return (decimal)(2 * numericUpDown1.Value + 2 * numericUpDown2.Value);
        }

        private void Calculate()
        {
            labelSquare.Text = (Square().ToString());
            labelPerimetr.Text = (Perimetr().ToString());
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            Calculate();
        }

        private void numericUpDown2_ValueChanged(object sender, EventArgs e)
        {
            Calculate();
        }
    }
}
