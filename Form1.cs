﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

namespace CourseWork
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            ListGovernment.governments = new List<Government>();
            ListDistrict.districts = new List<District>();
            ListCityArea.cityAreas = new List<CityArea>();
            ListLocality.localities = new List<Locality>();

            ShowTreeView();
        }

        // Функции построения treeView
        #region
        private bool FindNode(in TreeNodeCollection collection, in string name, out TreeNode node)
        {
            bool response = false;
            node = null;
            
            // найти treeNode с именем name и записать в node
            foreach (TreeNode treeNode in collection)
            {
                if (treeNode.Text == name)
                {
                    response = true;
                    node = treeNode;
                    break;
                }
            }

            return response;
        }

        private void AddTreeNodeGovern(Government government, out TreeNode treeGvn)
        {
            treeGvn = new TreeNode(government.GName);
            treeGvn.Tag = government;

            // проверить, нет ли такого государства в дереве
            if (!FindNode(treeView.Nodes, government.GName, out TreeNode tree))
                treeView.Nodes.Add(treeGvn);
            // вернуть существующий узел государства в дереве
            else
            {
                treeGvn = tree;
                treeGvn.Tag = tree.Tag;
            }
        }

        private void AddTreeNodeDistrict(District district, out TreeNode treeDistrict)
        {
            treeDistrict = new TreeNode(district.DistrictName);
            treeDistrict.Tag = district;

            // найти узел государства с именем district.GName
            if (FindNode(treeView.Nodes, district.GName, out TreeNode tree))
            {
                // проверить, нет ли такого субъекта в дереве
                if (!FindNode(tree.Nodes, district.DistrictName, out TreeNode treeD))
                    tree.Nodes.Add(treeDistrict);
                else
                {
                    treeDistrict = treeD;
                    treeDistrict.Tag = treeD.Tag;
                }

                return;
            }

            Government government = ListGovernment.GetByName(district.GName);
            AddTreeNodeGovern(government, out tree);
            tree.Nodes.Add(treeDistrict);
        }

        private void AddTreeNodeCityArea(CityArea cityArea, out TreeNode treeCityArea)
        {
            treeCityArea = new TreeNode(cityArea.CityName);
            treeCityArea.Tag = cityArea;

            // найти узел государства с именем cityArea.GName
            if (FindNode(treeView.Nodes, cityArea.GName, out TreeNode tree))
            {
                // найте узел субъекта с именем cityArea.DistrictName
                if (FindNode(tree.Nodes, cityArea.DistrictName, out tree))
                {
                    // проверить, нет ли такого района в дереве
                    if (!FindNode(tree.Nodes, cityArea.CityName, out TreeNode treeCity))
                        tree.Nodes.Add(treeCityArea);
                    else
                    {
                        treeCityArea = treeCity;
                        treeCityArea.Tag = treeCity.Tag;
                    }

                    return;
                }
            }

            // создать узлы 1 и 2 уровней
            District district = ListDistrict.GetByName(cityArea.DistrictName);
            AddTreeNodeDistrict(district, out tree);
            tree.Nodes.Add(treeCityArea);
        }

        private void AddTreeNodeLocality(Locality locality, out TreeNode treeLocality)
        {
            treeLocality = new TreeNode(locality.LocName);
            treeLocality.Tag = locality;

            // найти узел государства с именем cityArea.GName
            if (FindNode(treeView.Nodes, locality.GName, out TreeNode tree))
            {
                // найте узел субъекта с именем locality.DistrictName
                if (FindNode(tree.Nodes, locality.DistrictName, out tree))
                {
                    // найти узел района с именем locality.CityName
                    if (FindNode(tree.Nodes, locality.CityName, out tree))
                    {
                        tree.Nodes.Add(treeLocality);
                        return;
                    }

                }
            }

            // создать узлы 1, 2 и 3 уровней
            CityArea cityArea = ListCityArea.GetByName(locality.CityName);
            AddTreeNodeCityArea(cityArea, out tree);
            tree.Nodes.Add(treeLocality);
        }

        private void ShowTreeView()
        {
            treeView.Nodes.Clear();
            TreeNode treeBuf;
            foreach (Government item in ListGovernment.governments)
                AddTreeNodeGovern(item, out treeBuf);
            foreach (District item in ListDistrict.districts)
                AddTreeNodeDistrict(item, out treeBuf);
            foreach (CityArea item in ListCityArea.cityAreas)
                AddTreeNodeCityArea(item, out treeBuf);
            foreach (Locality item in ListLocality.localities)
                AddTreeNodeLocality(item, out treeBuf);
        }
        #endregion

        #region
        private void ClearLists()
        {
            ListGovernment.governments.Clear();
            ListDistrict.districts.Clear();
            ListCityArea.cityAreas.Clear();
            ListLocality.localities.Clear();
        }
        private void SetInfo()
        {
            // очищает информацию об выбарнном узле
            labelStructure.Text = "";
            labelName.Text = "";
            labelSquare.Text = "";
            labelPopulation.Text = "";
            labelType.Text = "";

            if (treeView.SelectedNode == null)
                return;

            object obj = treeView.SelectedNode.Tag;
            if (obj != null)
            {
                switch (treeView.SelectedNode.Level)
                {
                    case 0:
                        Government government = (Government)obj;
                        labelStructure.Text = "государство";
                        labelName.Text = government.GName;
                        labelSquare.Text = government.GSquare.ToString();
                        labelPopulation.Text = government.GPopulation.ToString();
                        labelType.Text = "государство";
                        break;
                    case 1:
                        District district = (District)obj;
                        labelStructure.Text = "субъект";
                        labelName.Text = district.DistrictName;
                        labelSquare.Text = district.DistrictSquare.ToString();
                        labelPopulation.Text = district.DistrictPopulation.ToString();
                        labelType.Text = "субъект";
                        break;
                    case 2:
                        CityArea cityArea = (CityArea)obj;
                        labelStructure.Text = "район";
                        labelName.Text = cityArea.CityName;
                        labelSquare.Text = cityArea.CitySquare.ToString();
                        labelPopulation.Text = cityArea.CityPopulation.ToString();
                        labelType.Text = "район";
                        break;
                    case 3:
                        Locality locality = (Locality)obj;
                        labelStructure.Text = "населенный пункт";
                        labelName.Text = locality.LocName;
                        labelSquare.Text = locality.LocSquare.ToString();
                        labelPopulation.Text = locality.LocPopulation.ToString();
                        labelType.Text = locality.LocType;
                        break;
                }
            }
        }

        private void DeleteTreeNode(TreeNode treeNode)
        {
            treeNode.Nodes.Clear();
            treeNode.Remove();
        }


        #endregion

        private void button1_Click(object sender, EventArgs e)
        {
            FormGovernment form = new FormGovernment();
            form.Owner = this;
            form.ShowDialog();

            if (DialogResult.OK == form.DialogResult)
            {
                ListGovernment.governments.Add(form.government);
                AddTreeNodeGovern(form.government, out TreeNode tree);
            }
            SetInfo();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (ListGovernment.governments.Count == 0)
            {
                MessageBox.Show("Нельзя добавлять субъекты, если нет ни одного государства!");
                return;
            }

            FormSubject form = new FormSubject();
            form.Owner = this;
            form.ShowDialog();

            if (DialogResult.OK == form.DialogResult)
            {
                ListDistrict.districts.Add(form.District);
                AddTreeNodeDistrict(form.District, out TreeNode treeDistrict);
            }
            SetInfo();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (ListDistrict.districts.Count == 0)
            {
                MessageBox.Show("Нельзя добавлять районы, если нет ни одного субъекта!");
                return;
            }

            FormArea form = new FormArea();
            form.Owner = this;
            form.ShowDialog();

            if (DialogResult.OK == form.DialogResult)
            {
                ListCityArea.cityAreas.Add(form.cityArea);
                AddTreeNodeCityArea(form.cityArea, out TreeNode tree);
            }
            SetInfo();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (ListCityArea.cityAreas.Count == 0)
            {
                MessageBox.Show("Нельзя добавлять населенные пункты, если нет ни одного района!");
                return;
            }

            FormLocality form = new FormLocality();
            form.Owner = this;
            form.ShowDialog();

            if (DialogResult.OK == form.DialogResult)
            {
                ListLocality.localities.Add(form.locality);
                AddTreeNodeLocality(form.locality, out TreeNode tree);
            }
            SetInfo();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            // Обработка кнопки "Редактировать"
            if (treeView.SelectedNode == null)
                return;

            switch (treeView.SelectedNode.Level)
            {
                case 0:
                    Government government = (Government)treeView.SelectedNode.Tag;
                    FormGovernment formGvn = new FormGovernment(government);
                    formGvn.Owner = this;
                    formGvn.ShowDialog();
                    if (DialogResult.OK == formGvn.DialogResult)
                    {
                        ListGovernment.governments.Add(formGvn.government);
                        ListGovernment.governments.Sort();
                        treeView.SelectedNode.Text = formGvn.government.GName;
                        treeView.SelectedNode.Tag = formGvn.government;
                    }
                    break;
                case 1:
                    District district = (District)treeView.SelectedNode.Tag;
                    FormSubject formSubject = new FormSubject(district);
                    formSubject.Owner = this;
                    formSubject.ShowDialog();
                    if (DialogResult.OK == formSubject.DialogResult)
                    {
                        ListDistrict.districts.Add(formSubject.District);
                        ListDistrict.districts.Sort();
                        treeView.SelectedNode.Text = formSubject.District.DistrictName;
                        treeView.SelectedNode.Tag = formSubject.District;
                    }
                    break;
                case 2:
                    CityArea city = (CityArea)treeView.SelectedNode.Tag;
                    FormArea formArea = new FormArea(city);
                    formArea.Owner = this;
                    formArea.ShowDialog();
                    if (DialogResult.OK == formArea.DialogResult)
                    {
                        ListCityArea.cityAreas.Add(formArea.cityArea);
                        ListCityArea.cityAreas.Sort();
                        treeView.SelectedNode.Text = formArea.cityArea.CityName;
                        treeView.SelectedNode.Tag = formArea.cityArea;
                    }
                    break;
                case 3:
                    Locality locality = (Locality)treeView.SelectedNode.Tag;
                    FormLocality formLocality = new FormLocality(locality);
                    formLocality.Owner = this;
                    formLocality.ShowDialog();
                    if (DialogResult.OK == formLocality.DialogResult)
                    {
                        ListLocality.localities.Add(formLocality.locality);
                        ListLocality.localities.Sort();
                        treeView.SelectedNode.Text = formLocality.locality.LocName;
                        treeView.SelectedNode.Tag = formLocality.locality;
                    }
                    break;
            }
            SetInfo();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            // Обработка кнопки "Удалить"
            if (treeView.SelectedNode == null)
                return;

            DialogResult dialog;
            switch (treeView.SelectedNode.Level)
            {
                case 0:
                    Government government = (Government)treeView.SelectedNode.Tag;
                    dialog = MessageBox.Show("ВНИМАНИЕ! При удалении государства будут " +
                        "удалены все субъекты, районы и населенные пункты удаляемого государства.",
                        "Удаление государства", MessageBoxButtons.YesNo, MessageBoxIcon.Warning,
                        MessageBoxDefaultButton.Button2);
                    if (dialog == DialogResult.Yes)
                    {
                        ListGovernment.DeleteGvn(government);
                        DeleteTreeNode(treeView.SelectedNode);
                    }
                    break;
                case 1:
                    District district = (District)treeView.SelectedNode.Tag;
                    dialog = MessageBox.Show("ВНИМАНИЕ! При удалении субъекта будут " +
                        "удалены все районы и населнные пункты удаляемого субъекта.",
                        "Удаление субъекта", MessageBoxButtons.YesNo, MessageBoxIcon.Warning,
                        MessageBoxDefaultButton.Button2);
                    if (dialog == DialogResult.Yes)
                    {
                        ListDistrict.DeleteDistrict(district);
                        DeleteTreeNode(treeView.SelectedNode);
                    }
                    break;
                case 2:
                    CityArea cityArea = (CityArea)treeView.SelectedNode.Tag;
                    dialog = MessageBox.Show("ВНИМАНИЕ! При удалении района будут " +
                        "удалены все его населнные пункты!", "Удаление района",
                        MessageBoxButtons.YesNo, MessageBoxIcon.Warning,
                        MessageBoxDefaultButton.Button2);
                    if (dialog == DialogResult.Yes)
                    {
                        ListCityArea.DeleteCityArea(cityArea);
                        DeleteTreeNode(treeView.SelectedNode);
                    }
                    break;
                case 3:
                    dialog = MessageBox.Show("Вы уверены, что хотите удалить населенный пункт",
                        "Удаление района", MessageBoxButtons.YesNo, MessageBoxIcon.Warning,
                        MessageBoxDefaultButton.Button2);
                    if (dialog == DialogResult.Yes)
                    {
                        Locality localicty = (Locality)treeView.SelectedNode.Tag;
                        ListLocality.DeleteLocality(localicty);
                        treeView.SelectedNode.Remove();
                    }
                    break;
            }
            SetInfo();
        }

        private void treeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            SetInfo();
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            treeView.Nodes.Clear();
            ClearLists();
            try
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.InitialDirectory = "c:\\";
                openFileDialog.Filter = "Файлы *.xml|*.xml";
                openFileDialog.FilterIndex = 2;
                openFileDialog.RestoreDirectory = true;
                if (openFileDialog.ShowDialog() == DialogResult.Cancel)
                    return;

                DataSerializer.LoadXML(openFileDialog.FileName);
                if (!ListDistrict.CheckIntegrityLink() ||
                    !ListCityArea.CheckIntegrityLink() ||
                    !ListLocality.CheckIntegrityLink())
                {
                    MessageBox.Show("Целостность данных в файле нарушена. Данные не будут загружены!",
                        "Открытие файла", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    ListGovernment.governments.Clear();
                    ListDistrict.districts.Clear();
                    ListCityArea.cityAreas.Clear();
                    ListLocality.localities.Clear();
                    return;
                }
                ShowTreeView();
            }
            catch (Exception error)
            {
                MessageBox.Show(error.Message);
            }
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();

            saveFileDialog.Filter = "Файлы *.xml|*.xml";
            saveFileDialog.FilterIndex = 2;
            saveFileDialog.RestoreDirectory = true;

            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                DataSerializer.SaveXML(saveFileDialog.FileName);
            }
        }

        private void treeView_DragDrop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop) && e.Effect == DragDropEffects.Move)
            {
                string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
                // В files хранятся пути к папкам и файлам
                if (files.Length > 1)
                {
                    _ = MessageBox.Show("Перетащите 1 файл");
                    return;
                }

                if (AnalizeFile(files[0]))
                {
                    DataSerializer.LoadXML(files[0]);
                    ShowTreeView();
                }
            }
        }

        private void treeView_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop) &&
                ((e.AllowedEffect & DragDropEffects.Move) == DragDropEffects.Move))
                e.Effect = DragDropEffects.Move;
        }

        private bool AnalizeFile(string fileName)
        {
            if (Path.GetExtension(fileName) != ".xml")
            {
                _ = MessageBox.Show("Выберите файл с расширением \".xml\"");
                return false;
            }
            return true;
        }
    }
}
