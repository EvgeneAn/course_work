﻿using System;

namespace CourseWork
{
    public class Government: IComparable
    {
        public string GName { get; set; }
        public int GSquare { get; set; } // площадь государтсва
        public int GPopulation { get; set; } // население

        public Government() { }

        public Government(string name, int square)
        {
            GName = name;
            GSquare = square;
            GPopulation = 0;
        }
        public Government(Government government)
        {
            GName = government.GName;
            GSquare = government.GSquare;
            GPopulation = government.GPopulation;
        }

        /// <summary>
        /// Определяет, можно ли выделить площадь субъекту размером square.
        /// </summary>
        public bool TakeGovernmentArea(int square)
        {
            // найти сумму площадей субъектов в данном государстве
            int remains = ListDistrict.GetTotalSquare(this);
            remains = GSquare - remains; // вычесть эту сумму из площади государства

            // если предолженная площадь субъекта не превышает остатка
            if (remains >= square)
                return true;
            return false;
        }

        /// <summary>
        /// Увеличивает население в текущем экземпляре класса
        /// </summary>
        public virtual void AddThisPopulation(int population)
        {
            GPopulation += population;
        }

        /// <summary>
        /// Определяет, сколько осталось нераспределенной площади у государства
        /// </summary>
        public int GetGvnRemains()
        {
            // найти сумму площадей н.п. расположенных в районе
            int remains = ListDistrict.GetTotalSquare(this);
            return GSquare - remains; // определить остаток площади района
        }

        public virtual int CompareTo(object o)
        {
            Government gvn = o as Government;
            if (gvn != null)
                return GName.CompareTo(gvn.GName);
            else
                throw new Exception("Невозможно сравнить 2 объекта");
        }

        /// <summary>
        /// Пересчитывает население, выполняя поиск всех своих городов и
        /// получая их населения.
        /// </summary>
        public virtual void RecountPopulation()
        {
            GPopulation = 0;
            foreach (Locality locality in ListLocality.localities)
                if (locality.GName == GName)
                    GPopulation += locality.LocPopulation;
        }
    }

    public class District : Government
    {
        // Класс для сущности субъектов государства, таких как область, край,
        // автономный округ и т.п.
        public string DistrictName { get; set; } // наименование субъекта
        public int DistrictSquare { get; set; } // площадь субъекта
        public int DistrictPopulation { get; set; } // население субъекта

        public District() { }

        public District(District district)
        {
            GName = district.GName;
            GSquare = district.GSquare;
            GPopulation = district.GPopulation;
            DistrictName = district.DistrictName;
            DistrictSquare = district.DistrictSquare;
            DistrictPopulation = district.DistrictPopulation;
        }

        public District(Government government, string sName, int square)
            : base(government)
        {
            DistrictName = sName;
            DistrictPopulation = 0;

            if (government.TakeGovernmentArea(square))
                DistrictSquare = square;
            else
                DistrictSquare = 0;
     
        }

        public District(Government government, District district)
            : base(government)
        {
            DistrictName = district.DistrictName;
            DistrictSquare = district.DistrictSquare;
            DistrictPopulation = district.DistrictPopulation;
        }

        /// <summary>
        /// Определяет, можно ли выделить площадь району размером square.
        /// </summary>
        public bool TakeDistrictArea(int square)
        {
            // найти сумму площадей районов в данном субъекте
            int remains = ListCityArea.GetTotalSquare(this);
            remains = DistrictSquare - remains; // вычесть эту сумму из площади субъекта

            // если предолженная площадь района не превышает остатка
            if (remains >= square)
                return true;
            return false;
        }

        public override void AddThisPopulation(int population)
        {
            GPopulation += population;
            DistrictPopulation += population;
        }

        /// <summary>
        /// Определяет, сколько осталось нераспределенной площади у субъекта
        /// </summary>
        public int GetDistrictRemains()
        {
            // найти сумму площадей н.п. расположенных в районе
            int remains = ListCityArea.GetTotalSquare(this);
            return DistrictSquare - remains; // определить остаток площади района
        }

        public override int CompareTo(object o)
        {
            District district = o as District;
            if (district != null)
                return DistrictName.CompareTo(district.DistrictName);
            else
                throw new Exception("Невозможно сравнить 2 объекта");
        }
        public override void RecountPopulation()
        {
            DistrictPopulation = 0;
            foreach (Locality locality in ListLocality.localities)
                if (locality.GName == GName && locality.DistrictName == DistrictName)
                    DistrictPopulation += locality.LocPopulation;
        }
    }

    public class CityArea : District
    {
        // Класс для сущности городского района
        public string CityName { get; set; } // наименование района
        public int CitySquare { get; set; } // площадь района
        public int CityPopulation { get; set; } // население района

        public CityArea() { }

        public CityArea(CityArea cityArea)
        {
            GName = cityArea.GName;
            GSquare = cityArea.GSquare;
            GPopulation = cityArea.GPopulation;
            DistrictName = cityArea.DistrictName;
            DistrictSquare = cityArea.DistrictSquare;
            DistrictPopulation = cityArea.DistrictPopulation;
            CityName = cityArea.CityName;
            CitySquare = cityArea.CitySquare;
            CityPopulation = cityArea.CityPopulation;
        }

        public CityArea(Government government, District district, string name, int square)
            : base(government, district)
        {
            CityName = name;
            CityPopulation = 0;

            if (district.TakeDistrictArea(square))
                CitySquare = square;
            else
                CitySquare = 0;
        }

        public CityArea(Government government, District district, CityArea cityArea)
            : base(government, district)
        {
            CityName = cityArea.CityName;
            CitySquare = cityArea.CitySquare;
            CityPopulation = cityArea.CityPopulation;
        }

        /// <summary>
        /// Определяет, можно ли выделить площадь населенному пункту размером square.
        /// </summary>
        public bool TakeCityArea(int square)
        {
            // найти сумму площадей н.п. расположенных в данном районе
            int remains = ListLocality.GetTotalSquare(this);
            remains = CitySquare - remains; // определить остаток площади района

            // если предолженная площадь города не превышает остатка
            if (remains >= square)
                return true;
            return false;
        }

        public override void AddThisPopulation(int population)
        {
            GPopulation += population;
            DistrictPopulation += population;
            CityPopulation += population;
        }

        /// <summary>
        /// Определяет, сколько осталось нераспределенной площади у района
        /// </summary>
        public int GetCityRemains()
        {
            // найти сумму площадей н.п. расположенных в районе
            int remains = ListLocality.GetTotalSquare(this);
            return CitySquare - remains; // определить остаток площади района
        }

        public override int CompareTo(object o)
        {
            CityArea cityArea = o as CityArea;
            if (cityArea != null)
                return CityName.CompareTo(cityArea.CityName);
            else
                throw new Exception("Невозможно сравнить 2 объекта");
        }

        public override void RecountPopulation()
        {
            CityPopulation = 0;
            foreach (Locality locality in ListLocality.localities)
                if (locality.GName == GName && locality.DistrictName == DistrictName &&
                    locality.CityName == CityName)
                    CityPopulation += locality.LocPopulation;
        }
    }

    public class Locality : CityArea
    {
        // Класс для сущности городского района
        public string LocName { get; set; } // наименование населенного пункта
        public string LocType { get; set; } // тип населенного пункта (город, село)
        public int LocSquare { get; set; } // площадь населенного пункта
        public int LocPopulation { get; set; } // население населенного пункта

        public Locality(Locality locality)
        {
            GName = locality.GName;
            GSquare = locality.GSquare;
            GPopulation = locality.GPopulation;
            DistrictName = locality.DistrictName;
            DistrictSquare = locality.DistrictSquare;
            DistrictPopulation = locality.DistrictPopulation;
            CityName = locality.CityName;
            CitySquare = locality.CitySquare;
            CityPopulation = locality.CityPopulation;
            LocName = locality.LocName;
            LocSquare = locality.LocSquare;
            LocPopulation = locality.LocPopulation;
            LocType = locality.LocType;
        }

        public Locality(Government government, District district, CityArea cityArea,
            string name, string type, int square, int population) : base(government, district, cityArea)
        {
            LocName = name;
            LocType = type;
            AddThisPopulation(population);
            government.AddThisPopulation(population);
            district.AddThisPopulation(population);
            cityArea.AddThisPopulation(population);

            if (cityArea.TakeCityArea(square))
                LocSquare = square;
            else
                LocSquare = 0;
        }

        public override void AddThisPopulation(int population)
        {
            GPopulation += population;
            DistrictPopulation += population;
            CityPopulation += population;
            LocPopulation = population;
        }

        public override int CompareTo(object o)
        {
            Locality locality = o as Locality;
            if (locality != null)
                return LocName.CompareTo(locality.LocName);
            else
                throw new Exception("Невозможно сравнить 2 объекта");
        }
    }
}
