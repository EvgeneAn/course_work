﻿using System;
using System.Windows.Forms;

namespace CourseWork
{
    public partial class FormLocality : Form
    {
        public Locality locality { get; set; }
        private Locality oldLocality = null; // изменять только в FormLocality(Locality oldLocality)
        public FormLocality()
        {
            InitializeComponent();

            foreach (Government item in ListGovernment.governments)
                comboBoxG.Items.Add(item.GName);
            if (comboBoxG.Items.Count > 0)
                comboBoxG.SelectedIndex = 0;

            SetComboBoxSubject();
            SetComboBoxCityArea();
        }

        public FormLocality(Locality oldLoc)
        {
            InitializeComponent();

            foreach (Government item in ListGovernment.governments)
                comboBoxG.Items.Add(item.GName);

            SetComboBoxSubject();
            SetComboBoxCityArea();

            // заполнение полей первоночальными данными
            for (int i = 0; i < comboBoxG.Items.Count; ++i)
                if (comboBoxG.Items[i].ToString() == oldLoc.GName)
                {
                    comboBoxG.SelectedIndex = i;
                    break;
                }
            for (int i = 0; i < comboBoxSubject.Items.Count; ++i)
                if (comboBoxSubject.Items[i].ToString() == oldLoc.DistrictName)
                {
                    comboBoxSubject.SelectedIndex = i;
                    break;
                }
            for (int i = 0; i < comboBoxCityArea.Items.Count; ++i)
                if (comboBoxCityArea.Items[i].ToString() == oldLoc.CityName)
                {
                    comboBoxCityArea.SelectedIndex = i;
                    break;
                }
            textBoxName.Text = oldLoc.LocName;
            for (int i = 0; i < comboBoxType.Items.Count; ++i)
                if (comboBoxType.Items[i].ToString() == oldLoc.LocType)
                {
                    comboBoxType.SelectedIndex = i;
                    break;
                }
            numericUpDownSquare.Value = oldLoc.LocSquare;
            numericUpDownPopulation.Value = oldLoc.LocPopulation;

            // сделать некоторые поля неизменяемыми
            comboBoxG.Enabled = false;
            comboBoxSubject.Enabled = false;
            comboBoxCityArea.Enabled = false;

            // Вычесть население, т.к. при успешном редактирование будет создан
            // новый экземпляр класса, у которого будет занаво расчитана площадь
            // и население
            oldLocality = new Locality(oldLoc);
            Government gvn = ListGovernment.GetByName(oldLoc.GName);
            District district = ListDistrict.GetByName(oldLoc.DistrictName);
            CityArea cityArea = ListCityArea.GetByName(oldLoc.CityName);
            cityArea.AddThisPopulation(-oldLoc.LocPopulation);
            district.AddThisPopulation(-oldLoc.LocPopulation);
            gvn.AddThisPopulation(-oldLoc.LocPopulation);
            ListLocality.localities.Remove(oldLoc);
        }

        private void SetComboBoxSubject()
        {
            comboBoxSubject.Items.Clear();
            comboBoxSubject.Text = "";
            object obj = comboBoxG.SelectedItem;

            if (obj != null)
            {
                Government government = ListGovernment.GetByName(obj.ToString());
                foreach (District item in ListDistrict.districts)
                    if (item.GName == government.GName)
                        comboBoxSubject.Items.Add(item.DistrictName);
                if (comboBoxSubject.Items.Count > 0)
                    comboBoxSubject.SelectedIndex = 0;
            }
        }

        private void SetComboBoxCityArea()
        {
            comboBoxCityArea.Items.Clear();
            comboBoxCityArea.Text = "";
            object obj = comboBoxSubject.SelectedItem;

            if (obj != null)
            {
                District district = ListDistrict.GetByName(obj.ToString());
                foreach (CityArea item in ListCityArea.cityAreas)
                    if (item.DistrictName == district.DistrictName)
                        comboBoxCityArea.Items.Add(item.CityName);
                if (comboBoxCityArea.Items.Count != 0)
                    comboBoxCityArea.SelectedIndex = 0;
            }
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            string output;
            if (textBoxName.Text == "")
            {
                output = String.Format("Поле \"{0}\" не должно быть пустым!", labelName.Text);
                MessageBox.Show(output);
                return;
            }
            if (numericUpDownSquare.Value == 0)
            {
                output = String.Format("Поле \"{0}\" не должно быть пустым!", labelSquare.Text);
                MessageBox.Show(output);
                return;
            }
            if (numericUpDownPopulation.Value == 0)
            {
                output = String.Format("Поле \"{0}\" не должно быть пустым!", labelPopulation.Text);
                MessageBox.Show(output);
                return;
            }


            object obj = comboBoxG.SelectedItem;
            if (obj != null)
            {
                Government government = ListGovernment.GetByName(obj.ToString());

                obj = comboBoxSubject.SelectedItem;
                if (obj != null)
                {
                    District district = ListDistrict.GetByName(obj.ToString());

                    obj = comboBoxCityArea.SelectedItem;
                    if (obj != null)
                    {
                        CityArea cityArea = ListCityArea.GetByName(obj.ToString());

                        obj = comboBoxType.SelectedItem;
                        if (obj != null)
                        {
                            locality = new Locality(government, district, cityArea, textBoxName.Text,
                                obj.ToString(), (int)numericUpDownSquare.Value, (int)numericUpDownPopulation.Value);

                            if (locality.LocSquare == 0)
                            {
                                output = "Площадь населенного пункта превышает площадь района!" +
                                    String.Format(" Осталось нераспределенной площади {0}.",
                                    cityArea.GetCityRemains());
                                MessageBox.Show(output);
                                district.AddThisPopulation(-locality.LocPopulation);
                                government.AddThisPopulation(-locality.LocPopulation);
                                cityArea.AddThisPopulation(-locality.LocPopulation);
                                return;
                            }
                            DialogResult = DialogResult.OK;
                        }
                        else
                        {
                            output = String.Format("Убедитесь, что поле {0} заполнено!", labelType.Text);
                            MessageBox.Show(output);
                        }
                    }
                    else
                    {
                        output = String.Format("Убедитесь, что поле {0} заполнено!", labelCityArea.Text);
                        MessageBox.Show(output);
                    }
                }
                else
                {
                    output = String.Format("Убедитесь, что поле {0} заполнено!", labelSubject.Text);
                    MessageBox.Show(output);
                }

            }
            else
            {
                output = String.Format("Убедитесь, что поле {0} заполнено!", labelGovernment.Text);
                MessageBox.Show(output);
            }
        }

        private void comboBoxG_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetComboBoxSubject();
            SetComboBoxCityArea();
        }

        private void comboBoxSubject_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetComboBoxCityArea();
        }

        private void buttonCancle_Click(object sender, EventArgs e)
        {
            // если производилось редактирование и нажали "Отмена"
            // вернуть население из старых данных
            if (oldLocality != null)
            {
                locality = oldLocality;
                Government gvn = ListGovernment.GetByName(locality.GName);
                District district = ListDistrict.GetByName(locality.DistrictName);
                CityArea cityArea = ListCityArea.GetByName(locality.CityName);
                cityArea.AddThisPopulation(oldLocality.LocPopulation);
                district.AddThisPopulation(oldLocality.LocPopulation);
                gvn.AddThisPopulation(oldLocality.LocPopulation);
                ListLocality.localities.Add(oldLocality);
            }
        }
    }
}
