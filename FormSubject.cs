﻿using System;
using System.Windows.Forms;

namespace CourseWork
{
    public partial class FormSubject : Form
    {
        public District District { get; set; }
        private District OldDistrict = null;
        public FormSubject()
        {
            InitializeComponent();

            foreach (Government item in ListGovernment.governments)
                comboBoxG.Items.Add(item.GName);
            if (comboBoxG.Items.Count > 0)
                comboBoxG.SelectedIndex = 0;
        }

        public FormSubject(District district)
        {
            InitializeComponent();

            foreach (Government item in ListGovernment.governments)
                comboBoxG.Items.Add(item.GName);
            if (comboBoxG.Items.Count > 0)
                comboBoxG.SelectedIndex = 0;

            // заполнение полей первоночальными данными
            for (int i = 0; i < comboBoxG.Items.Count; ++i)
                if (comboBoxG.Items[i].ToString() == district.GName)
                {
                    comboBoxG.SelectedIndex = i;
                    break;
                }
            comboBoxG.Enabled = false;
            textBoxSubject.Text = district.DistrictName;
            numericUpDown1.Value = district.DistrictSquare;

            OldDistrict = new District(district);
            ListDistrict.districts.Remove(district);
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            string output;
            if (textBoxSubject.Text == "")
            {
                output = String.Format("Поле \"{0}\" не должно быть пустым!", label1.Text);
                MessageBox.Show(output);
                return;
            }
            if (numericUpDown1.Value == 0)
            {
                output = String.Format("Поле \"{0}\" не должно быть пустым!", label3.Text);
                MessageBox.Show(output);
                return;
            }
            if (OldDistrict != null)
            {
                int square = ListCityArea.GetTotalSquare(OldDistrict);
                if (numericUpDown1.Value < square)
                {
                    output = String.Format("Указанная площадь субъекта меньше, чем сумма площадей " +
                        "районов, расположенных в этом субъекте! Сумма площадей " +
                        "районов: {0}.", square);
                    MessageBox.Show(output, "Неверное значение поля");
                    return;
                }
            }

            object obj = comboBoxG.SelectedItem;
            if (obj != null)
            {
                Government government = ListGovernment.GetByName(obj.ToString());

                if (ListDistrict.CheckUnique(government.GName, textBoxSubject.Text))
                {
                    District = new District(government, textBoxSubject.Text, (int)numericUpDown1.Value);

                    if (District.DistrictSquare != 0)
                    {
                        if (OldDistrict != null)
                        {
                            District.AddThisPopulation(OldDistrict.DistrictPopulation);
                            ListCityArea.ResetCityData(District);
                            if (OldDistrict.DistrictName != District.DistrictName)
                            {
                                ListCityArea.ResetNameDistrict(OldDistrict, District);
                                ListLocality.ResetNameDistrict(OldDistrict, District);
                            }
                        }
                        DialogResult = DialogResult.OK;
                    }
                    else
                    {
                        output = "Площадь субъекта превышает площадь государства!" +
                            String.Format(" Осталось нераспределенной площади {0}.",
                            government.GetGvnRemains());
                        MessageBox.Show(output);
                    }
                }
                else
                {
                    output = "Субъект с таким наименованием уже существует в государстве" +
                        String.Format(" {0}!", government.GName);
                    MessageBox.Show(output);
                }
            }
            else
            {
                output = String.Format("Убедитесь, что поле {0} заполнено!", label2.Text);
                MessageBox.Show(output);
            }
        }

        private void buttonCancle_Click(object sender, EventArgs e)
        {
            // если производилось редактирование и нажали "Отмена"
            // вернуть население из старых данных
            if (OldDistrict != null)
            {
                District = OldDistrict;
                ListDistrict.districts.Add(District);
            }
        }
    }
}
